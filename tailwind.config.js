module.exports = {
  purge: {
    enabled: process.env.NODE_ENV === "production",
    mode: "postcss",
    content: ["./src/**/*.html", "./src/**/*.ts", "./src/**/*.tsx"],
    whitelist: ["body", "html", "svg"],
    whitelistPatterns: [/Toastify.+/],
  },
  future: {
    removeDeprecatedGapUtilities: true,
  },
}
