import React from "react"
import { DataType, GraphData } from "../types"
import { ForceGraph3D } from "react-force-graph"
import SpriteText from "three-spritetext"
import { TextureLoader, SpriteMaterial, Sprite } from "three"
import { client } from "../axios"

const pictures: Record<string, string | null> = {}

export const MusicGraph = ({
  height,
  width,
  data,
  add,
}: {
  height?: number
  width?: number
  data: GraphData
  add: (type: DataType, id: number) => Promise<void>
}) => {
  const getPic = async (
    n: {
      id: string
      type: "artist" | "music"
      real_id: number
    },
    name: string
  ) => {
    if (pictures[name] !== undefined) {
      return pictures[name]
    }
    type Datum = { name: string; url: string }
    try {
      const r = await client.get<{
        artists: Datum[]
        tracks: Datum[]
      }>(`/lastfm/search`, { params: { q: name } })
      let url: string =
        "https://lastfm.freetls.fastly.net/i/u/300x300/2a96cbd8b46e442fc41c2b86b821562f.png"
      if (n.type === "artist") {
        const a = r.data.artists.shift()
        if (a) {
          url = a.url
        }
      } else {
        const t = r.data.tracks.shift()
        if (t) {
          url = t.url
        }
      }
      pictures[name] = url
      return url
    } catch (e) {
      console.error(e)
      return "https://lastfm.freetls.fastly.net/i/u/300x300/2a96cbd8b46e442fc41c2b86b821562f.png"
    }
  }
  return (
    <ForceGraph3D
      height={height}
      linkColor="#1e40af"
      linkWidth={2}
      graphData={data}
      nodeRelSize={0}
      //nodeLabel="id"
      //nodeAutoColorBy="id"
      nodeThreeObjectExtend={true}
      onNodeClick={(node) => {
        const n = node as {
          id: string
          type: "artist" | "music"
          real_id: number
        }
        add(n.type, n.real_id)
      }}
      nodeThreeObject={(node) => {
        const n = node as {
          id: string
          type: "artist" | "music"
          real_id: number
        }
        const _ = n.id.split("-")
        _.shift()
        const name = _.join("")
        const sprite = new Sprite()
        const sprite2 = new SpriteText(name)
        sprite2.color = "black"
        sprite2.textHeight = 1.5
        sprite2.fontWeight = "600"
        sprite2.position.z += 10
        sprite2.backgroundColor = "white"
        sprite.add(sprite2)
        ;(async () => {
          const url = await getPic(n, name)
          if (url) {
            const imgTexture = new TextureLoader().load(url)
            const material = new SpriteMaterial({ map: imgTexture })
            const sprite3 = new Sprite(material)
            sprite3.scale.set(12, 12, 12)
            sprite.add(sprite3)
          }
        })()

        setTimeout(() => {}, 1000)

        return sprite
      }}
      linkPositionUpdate={(sprite, { start, end }) => {
        const middlePos = Object.assign(
          {},
          ...["x", "y", "z"].map((c) => ({
            // @ts-ignore
            [c]: start[c] + (end[c] - start[c]) / 2, // calc middle point
          }))
        )

        // Position sprite
        Object.assign(sprite.position, middlePos)
        return null
      }}
    />
  )
}
