import React, { useEffect, useState } from "react"
import { MusicGraph } from "./graph"
import { Search } from "./search"
import { client } from "../axios"
import {
  MusicDetail,
  ArtistDetail,
  DataType,
  ObjectLiteral,
  GraphLink,
  GraphNode,
} from "../types"

export const App: React.FC<{}> = () => {
  const [height, setHeight] = useState(window.innerHeight - 48)
  const [width, setWidth] = useState(window.innerWidth)
  window.addEventListener("resize", () => {
    if (height !== window.innerHeight - 48) setHeight(window.innerHeight - 48)
    if (width !== window.innerWidth) setWidth(window.innerWidth)
  })
  const [data, setData] = useState<{
    nodes: (GraphNode & ObjectLiteral & { type: DataType; real_id: number })[]
    links: (GraphLink & ObjectLiteral)[]
  }>({
    nodes: [],
    links: [],
  })

  /*useEffect(() => {
    const interval = setInterval(() => {
      if (0 < data.nodes.length) {
        setData((d) => ({
          ...d,
          nodes: [
            ...d.nodes.filter((n) => n.real_id !== 0),
            { id: `debug-${Math.random()}`, type: "artist", real_id: 0 },
          ],
        }))
      }
    }, 1000 * 2)
    return () => {
      clearInterval(interval)
    }
  }, [])*/

  const add = async (type: DataType, id: number) => {
    const copied = Object.assign({}, data)
    switch (type) {
      case "music":
        {
          const r = await client.get<MusicDetail>(`/${type}s/${id}`)
          const music = r.data
          if (!copied.nodes.find((n) => n.id === `${music.id}-${music.name}`)) {
            copied.nodes.push({
              type,
              id: `${music.id}-${music.name}`,
              real_id: music.id,
            })
          }

          music.artists.map((artist) => {
            if (
              !copied.nodes.find((n) => n.id === `${artist.id}-${artist.name}`)
            ) {
              copied.nodes.push({
                ...artist,
                type: "artist",
                id: `${artist.id}-${artist.name}`,
                real_id: artist.id,
              })
            }
            if (
              !copied.links.find(
                (l) =>
                  l.source === `${music.id}-${music.name}` &&
                  l.target === `${artist.id}-${artist.name}`
              )
            ) {
              copied.links.push({
                source: `${music.id}-${music.name}`,
                target: `${artist.id}-${artist.name}`,
              })
            }
          })
          setData(copied)
        }
        break
      case "artist":
        {
          const r = await client.get<ArtistDetail>(`/${type}s/${id}`)
          const artist = r.data
          if (
            !copied.nodes.find((n) => n.id === `${artist.id}-${artist.name}`)
          ) {
            copied.nodes.push({
              type,
              id: `${artist.id}-${artist.name}`,
              real_id: artist.id,
            })
          }

          artist.parents.map((parent) => {
            if (
              !copied.nodes.find((n) => n.id === `${parent.id}-${parent.name}`)
            ) {
              copied.nodes.push({
                ...parent,
                type: "artist",
                id: `${parent.id}-${parent.name}`,
                real_id: parent.id,
              })
            }
            if (
              !copied.links.find(
                (l) =>
                  l.source === `${artist.id}-${artist.name}` &&
                  l.target === `${parent.id}-${parent.name}`
              )
            ) {
              copied.links.push({
                source: `${artist.id}-${artist.name}`,
                target: `${parent.id}-${parent.name}`,
              })
            }
          })
          artist.children.map((parent) => {
            if (
              !copied.nodes.find((n) => n.id === `${parent.id}-${parent.name}`)
            ) {
              copied.nodes.push({
                ...parent,
                type: "artist",
                id: `${parent.id}-${parent.name}`,
                real_id: parent.id,
              })
            }
            if (
              !copied.links.find(
                (l) =>
                  l.source === `${artist.id}-${artist.name}` &&
                  l.target === `${parent.id}-${parent.name}`
              )
            ) {
              copied.links.push({
                source: `${artist.id}-${artist.name}`,
                target: `${parent.id}-${parent.name}`,
              })
            }
          })
          artist.musics.map((music) => {
            if (
              !copied.nodes.find((n) => n.id === `${music.id}-${music.name}`)
            ) {
              copied.nodes.push({
                ...parent,
                type: "music",
                id: `${music.id}-${music.name}`,
                real_id: music.id,
              })
            }
            if (
              !copied.links.find(
                (l) =>
                  l.source === `${artist.id}-${artist.name}` &&
                  l.target === `${music.id}-${music.name}`
              )
            ) {
              copied.links.push({
                source: `${artist.id}-${artist.name}`,
                target: `${music.id}-${music.name}`,
              })
            }
          })
          setData(copied)
        }
        break
      default:
        break
    }
  }

  return (
    <div className="min-h-screen w-full flex flex-col text-gray-800">
      <div className="flex-1">
        <div className=" bg-gray-800">
          <div className="flex items-center container mx-auto justify-between max-w-screen-md text-gray-200">
            <div className="flex items-center justify-start">
              <a href="/">
                <div className="flex items-center justify-start mx-4 my-3">
                  実験実装
                </div>
              </a>
            </div>
            <div className="flex items-center justify-end w-1/2">
              <Search add={add} />
            </div>
          </div>
        </div>
        <div className="mx-auto overflow-hidden">
          <MusicGraph height={height} width={width} data={data} add={add} />
        </div>
      </div>
    </div>
  )
}
