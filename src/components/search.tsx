import React, { useEffect, useState } from "react"
import { useDebounce } from "react-use"
import { client } from "../axios"
import { Artist, Music, DataType } from "../types"

type OptionsType = {
  type: DataType
  value: number
  label: string
}

export const Search = ({
  add,
}: {
  add: (type: DataType, id: number) => Promise<void>
}) => {
  const [isSuggestShow, setIsSuggestShow] = useState(false)
  const [q, setQ] = useState("")
  const [options, setOptions] = useState<OptionsType[]>([])

  useDebounce(
    () => {
      ;(async () => {
        try {
          const [a, m] = await Promise.all([
            client.get<Artist[]>(
              0 < q.length ? `/artists?q=${q}%` : `/artists?q=%`
            ),
            client.get<Music[]>(
              0 < q.length ? `/musics?q=${q}%` : `/musics?q=%`
            ),
          ])
          setOptions(
            [
              ...a.data.map((d) => ({
                value: d.id,
                label: d.name,
                type: "artist" as const,
              })),
              ...m.data.map((m) => ({
                value: m.id,
                label: m.name,
                type: "music" as const,
              })),
            ].sort()
          )
        } catch (e) {
          console.error(e)
          alert("読み込みに失敗しました（再読み込みします）")
          try {
            await client.get(`/artists?q=`)
          } catch (error) {}
          location.reload()
        }
      })()
    },
    400,
    [q]
  )
  return (
    <form className="w-full">
      <div className="flex items-center border-b border-teal-500 py-1 my-1">
        <input
          className="appearance-none bg-transparent border-none w-full text-gray-200 mr-3 py-1 px-2 leading-tight focus:outline-none"
          type="text"
          placeholder="楽曲名・またはアーティスト名を入力"
          onBlur={() => {
            setTimeout(() => setIsSuggestShow(false), 100)
          }}
          onFocus={() => setIsSuggestShow(true)}
          onChange={(e) => setQ(e.target.value)}
          value={q}
        />
      </div>
      <div
        className="w-full max-w-sm rounded shadow-md my-2 top-0 right-0 absolute z-10 overflow-hidden"
        style={{
          top: "3rem",
          right: "15%",
          display: isSuggestShow ? "inherit" : "none",
        }}
      >
        <ul className="list-reset overflow-auto" style={{ maxHeight: "80vh" }}>
          {options.map((option, idx) => (
            <li
              className="bg-gray-100 hover:bg-gray-300"
              key={idx}
              onClick={async () => {
                setQ("")
                setIsSuggestShow(false)
                await add(option.type, option.value)
              }}
            >
              <p className="p-2 block text-black hover:bg-grey-light cursor-pointer">
                {option.type === "artist" ? "[Artist] " : "[Music] "}
                {option.label}
              </p>
            </li>
          ))}
        </ul>
      </div>
    </form>
  )
}
