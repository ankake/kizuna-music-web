import axios from "axios"
import { cacheAdapterEnhancer, throttleAdapterEnhancer } from "axios-extensions"

const MAX_REQUESTS_COUNT = 5
const INTERVAL_MS = 100
let PENDING_REQUESTS = 0

export const client = axios.create({
  baseURL: process.env.API_HOST,
  adapter: cacheAdapterEnhancer(axios.defaults.adapter!, {
    enabledByDefault: false,
  }),
})

client.interceptors.request.use((config) => {
  return new Promise((resolve, reject) => {
    let interval = setInterval(() => {
      if (PENDING_REQUESTS < MAX_REQUESTS_COUNT) {
        PENDING_REQUESTS++
        clearInterval(interval)
        resolve(config)
      }
    }, INTERVAL_MS)
  })
})
client.interceptors.response.use(
  function (response) {
    PENDING_REQUESTS = Math.max(0, PENDING_REQUESTS - 1)
    return Promise.resolve(response)
  },
  function (error) {
    PENDING_REQUESTS = Math.max(0, PENDING_REQUESTS - 1)
    return Promise.reject(error)
  }
)
