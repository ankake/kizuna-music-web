import React, { useEffect, useState } from "react"
import ReactDOM from "react-dom"

import { App } from "./components/app"

ReactDOM.render(<App />, document.getElementById("app"))
