export type DataType = "artist" | "music"

export type ObjectLiteral = { [key: string]: any }

export type Music = {
  id: number
  name: string
}

export type Artist = {
  id: number
  type: "individual" | "group"
  name: string
}

export type ArtistDetail = Artist & {
  parents: Artist[]
  children: Artist[]
  musics: Music[]
}

export type MusicDetail = Music & {
  artists: Artist[]
}

export type GraphNode = { id: string }

export type GraphLink = { source: string; target: string }

export type GraphData = {
  nodes: GraphNode[]
  links: GraphLink[]
}
